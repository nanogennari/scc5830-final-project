# SCC5830 - Final project

Juliano Genari de Araújo - USP number 13700244

## Introduction

The main goal of this project id to create an algorithm to segregate, reasonable well lit, Printed Circuit Boards (PCBs) on to trails, components, silkscreen, etc, and generate high contrast images to help with reverse engineering tasks.

## Used libraries

* numpy
* matplotlib
* imageio
* cv2
* seaborn
* skimage

## Files

* `test_images\`: Folder containing test images.
* `test_images\`: Folder containing test outputs.
* `presentation.ipynb`: Examples and algorithm explanation.

## Preliminary Results

In preliminary tests we were able to segregate in some cases the solder mask and the background using only the hue data of the image, without any preliminary information of the color of solder mask used in the PCB.

![Green PCB segregated.](/preliminary_green.png)
![Red PCB segregated.](/preliminary_red.png)

## Final results

At the end of the project some success was acheved, segregation of PCB images where acheived for images with the right condition:

![Green PCB segregated.](/test_outputs/green_1.jpg)
![Red PCB segregated.](/test_outputs/red_1.jpg)

The initial problem proved to be far more difficult than expected resulting in not all initial goals been acheived:

* The implemented algorith is verry sensible to uneven light;
* If the background and pcb have similar hue the backgroung segregation can not be done and the PCB segregation will show poor results without previous cropping by the user;
* Segregation between pads and silk mask and segregation of components is not possible with the proposed algorithm;

## Presentation

Presentation can be found here: https://youtu.be/WjaMcj2KW6Q

## Acknowledgments

This project is the final project of the discipline SCC5830 - Image Processing and Analysis (2022/1) as a part of the Computer Science and Computational Mathematics Master's program at ICMC-USP.

Test images where selected from the EEVblog flickr account (https://www.flickr.com/photos/eevblog/) and belong to Dave Jones.