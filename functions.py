import numpy as np
import matplotlib as mpl
from skimage import morphology
from typing import Any, Callable, List, Tuple
import matplotlib.pyplot as plt
import cv2
import seaborn as sns

CROP_MSG = "if the image has a not smooth background please crop it to the PCB"


def hue_histogram(img: np.ndarray, nbins: int = 256) -> Tuple[np.ndarray, np.ndarray]:
    """Generates an histogram of the hue channel for the image.

    Args:
        img (np.ndarray): Image.
        nbins (int, optional): Number of divisions. Defaults to 256.

    Returns:
        Tuple[np.ndarray, np.ndarray]: Histogram values, and bins limits.
    """
    img_hsv = mpl.colors.rgb_to_hsv(img)
    img_hue = img_hsv[:, :, 0]
    return np.histogram(img_hue, bins=nbins)


def select_hue_range(img: np.ndarray, min_hue: int, max_hue: int) -> np.ndarray:
    """Select an mask of an image of pixels inside a hue range.

    Args:
        img (np.ndarray): Image.
        min_hue (int): Minimum hue value.
        max_hue (int): Maximum hue value.

    Returns:
        np.ndarray: Mask of the region inside hue range.
    """
    img_hsv = mpl.colors.rgb_to_hsv(img)
    img_hue = img_hsv[:, :, 0]

    mask_hue = (img_hue > min_hue) & (img_hue < max_hue)

    return ~mask_hue


def sequence_to_ranges(l: list) -> list:
    """Converts a list of values to a list of ranges:

    [1, 2, 3, 5, 7, 8, 9] -> [(1, 3), (5, 5), (7, 9)]

    Args:
        l (list): List of values.

    Returns:
        list: List of ranges.
    """
    if len(l) > 0:
        l.sort()
        first_v = l[0]
        last_v = l[0]
        ranges = []
        for v in l[1:]:
            if v > last_v + 1:
                ranges.append((first_v, last_v))
                first_v = v
            last_v = v

        ranges.append((first_v, last_v))

        return ranges


def border_mean(mask_img: np.ndarray) -> np.ndarray:
    """Calculates the mean value of pixels in the border of an image.

    Args:
        mask_img (np.ndarray): Image.

    Returns:
        np.ndarray: Mean value of the border pixels.
    """
    border_list = [
        mask_img[0, :],
        mask_img[1:, -1],
        mask_img[-1, 1:],
        mask_img[1:-1, 0],
    ]
    border = np.concatenate(border_list)
    return np.mean(border)


def get_board_mask(
    img: np.ndarray,
    hist_function: Callable = hue_histogram,
    show_histograms: bool = False,
    return_segregation_masks: bool = False,
    return_final_masks: bool = False,
    figsize: Tuple[float, float] = (10, 8),
) -> Tuple[np.ndarray, bool]:
    """Tries to find an mask for an PCB in na image isolating the background.

    Args:
        img (np.ndarray): Image of an PCB.
        hist_function (Callable, optional): Histogram function to isolate peaks. Defaults to hue_histogram.

    Returns:
        Tuple[np.ndarray, bool]: Mask and boolean if mask was found.
    """
    # Apply median blur to image to reduce edge noise
    img = cv2.medianBlur(img, 5)

    # Increase the number of bins up to the last one with two distinct peaks
    # in the hue histogram
    last_peaks = 0
    for nbins in [8, 12, 16, 24, 32, 48, 64, 96, 128, 192, 256]:
        hist, new_x = hist_function(img, nbins=nbins)
        if show_histograms:
            plt.figure(figsize=figsize)
            plt.title(f"nbins = {nbins}")
            plt.bar(new_x[1:], hist, width=1 / len(new_x))
            plt.show()
        peaks = np.arange(len(hist))[hist > np.mean(hist)]
        new_ranges = sequence_to_ranges(peaks)
        if len(peaks) > 2 and last_peaks == 2:
            break
        x = new_x
        ranges = new_ranges
        last_peaks = len(peaks)
        if len(peaks) > 2:
            break
    if show_histograms:
        return None

    # Create images with the segments of hue and calculate percentage of borders.
    border_p = []
    area = []
    mask_segmets = []
    for i, j in ranges:
        mask_this = select_hue_range(img, x[i], x[j + 1])
        area.append(np.mean(~mask_this))
        mask_segmets.append(mask_this)
        border_p.append(border_mean(mask_this))
    if return_segregation_masks:
        return mask_segmets

    # Background is the one with the higher border percentage
    if len(ranges) > 1:
        bg_selected = True
        bg_i = np.argmin(border_p)
        bg_p = border_p.pop(bg_i)
        area.pop(bg_i)
        mask_segmets.pop(bg_i)
        if bg_p > 0.5:
            print(f"Selected background might not be correct, {CROP_MSG}.")
    else:
        bg_selected = False
        print(f"Unable to find background, {CROP_MSG}.")
        mask_segmets = mask_segmets[0].reshape([1] + list(mask_segmets[0].shape))

    # Solder mask is the one with the highest area
    sm_i = np.argmax(area)
    if np.max(area) < 0.3:
        bg_selected = False
        sm_mask = mask_segmets[sm_i] < 0
        print(f"Unable to find background, {CROP_MSG}.")
    else:
        sm_mask = mask_segmets[sm_i]

    # Apply closing operantion
    sm_mask = morphology.binary_opening(sm_mask, morphology.disk(5)).astype(bool)

    # Fill mask from the border
    new_sm_mask = np.zeros(sm_mask.shape, dtype=np.uint8)
    mask = np.pad((~sm_mask).astype(np.uint8), (1, 1), mode="edge")
    new_sm_mask = (cv2.floodFill(new_sm_mask, mask, (0, 0), 1)[1]).astype(bool)
    if return_final_masks:
        return [sm_mask, new_sm_mask]

    return new_sm_mask, bg_selected


def apply_mask(
    image: np.ndarray, mask: np.ndarray, empty_values: int = 0
) -> np.ndarray:
    """Applies an mask to an image

    Args:
        image (np.ndarray): Image array.
        mask (np.ndarray): Mask array.
        empty_values (int): Values to fill outside mask. Defaults to 0.

    Returns:
        np.ndarray: Masked iamge.
    """
    new_image = empty_values * np.ones(image.shape, dtype=image.dtype)
    new_image[~mask] = image[~mask]

    return new_image


def plot_images(
    images: List[np.ndarray],
    layout: Tuple[int, int],
    cmaps: Any = None,
    titles: Any = None,
    show: bool = True,
    figsize: Tuple[float, float] = (10, 6),
) -> None:
    """Plot a sequence of images.

    Args:
        images (List[np.ndarray]): Images to plot.
        layout (Tuple[int, int]): Layout to plot.
        titles (Any, optional): Titles of images. Defaults to None.
        cmaps (Any, optional): Color maps of images. Defaults to None.
        show (bool, optional): Show plot. Defaults to True.
        figsize (Tuple[float, float], optional): Figzise to plot. Defaults to (10, 6).
    """

    # Check if there id enough positions
    if (layout[0] * layout[1]) < len(images):
        print(f"Layout {layout} is too small to plot {len(images)} images.")
        images = images[: (layout[0] * layout[1])]

    fig = plt.figure(figsize=figsize)
    for i, image in enumerate(images):
        plt.subplot(layout[0], layout[1], i + 1)
        try:
            plt.title(titles[i])
        except (TypeError, IndexError):
            pass
        kwargs = {}
        try:
            kwargs["cmap"] = cmaps[i]
        except (TypeError, IndexError):
            pass
        plt.imshow(image, **kwargs)
    if show:
        plt.show()
    return fig


def segregate_image(
    image: np.ndarray, plot_mask: bool = False, color_seed: int = 0
) -> np.ndarray:

    # Try to get the board mask
    board_mask, found_mask = get_board_mask(image)

    M, N, _ = image.shape

    # Mask image
    if found_mask:
        masked_image = apply_mask(image, board_mask, 0)
    else:
        masked_image = image

    # Plot intermediary images
    if plot_mask:
        plot_images(
            [image, board_mask, masked_image],
            (1, 3),
            figsize=(20, 15),
            cmaps=[None, "gray", None],
            titles=["Original image", "Board mask", "Masked board"],
        )

    # Select data for kmeans
    data = mpl.colors.rgb_to_hsv(masked_image)[:, :, :]
    data = data[~board_mask].reshape((-1, 3)).astype(np.float32)
    k = 3 if found_mask else 4
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 20, 0.01)

    # Run Kmeans
    _, labels, _ = cv2.kmeans(data, k, None, criteria, 20, cv2.KMEANS_RANDOM_CENTERS)

    # Contruct labels image
    labels_img = np.zeros((M, N), dtype=np.uint8)
    labels_img[~board_mask] = labels.flatten() + 1

    # Contruct high contrast image
    color_img = np.zeros((M, N, 3), dtype=float)
    colors = [
        sns.color_palette("bright")[0],
        sns.color_palette("bright")[1],
        sns.color_palette("bright")[2],
        sns.color_palette("bright")[4],
    ]
    if color_seed != 0:
        rng = np.random.default_rng(color_seed)
        rng.shuffle(colors)
    if found_mask:
        colors = [(1.0, 1.0, 1.0)] + colors
    for i in np.unique(labels_img):
        color = colors.pop(0)
        color_img[labels_img == i] = color

    return color_img
